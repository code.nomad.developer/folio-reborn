import React from "react";
import {
  FaLinkedin,
  FaTwitter,
  FaGithub,
  FaInstagram,
  FaEnvelope,
} from "react-icons/fa";

export default function Footer() {
  return (
    <footer className="footer bg-gray-800 text-white">
      <div className="container mx-auto px-2 py-4">
        <section className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-8">
          <article className="col-span-1 md:col-span-1 lg:col-span-1">
            <div className="mb-1">
              <h3 className="font-bold text-xl">Marcin Code-Nomad</h3>
              <p className="text-gray-400">Front-End Developer & Tech Leader</p>
            </div>
            <a href="/portfolio" className="btn btn-primary font-bold">
              Explore the work
            </a>
          </article>
          <article className="col-span-1 md:col-span-1 lg:col-span-1">
            <div className="mb-4">
              <h4 className="font-bold text-xl">Contact</h4>
              <ul className="footer-contact-list mt-4">
                <li className="flex items-center">
                  <FaEnvelope className="text-gray-400 mr-2" />
                  <a
                    href="mailto:code.nomad.developer@gmail.com"
                    className="text-white"
                  >
                    code.nomad.developer@gmail.com
                  </a>
                </li>
              </ul>
            </div>
          </article>
          <aside className="col-span-1 md:col-span-1 lg:col-span-1 justify-self-end">
            <div className="mb-4 text-right">
              <h4 className="font-bold text-xl">Social media</h4>
              <ul className="social-media-links mt-4 flex justify-around">
                <li>
                  <a
                    href="https://www.linkedin.com/in/johndoe/"
                    target="_blank"
                    className="text-gray-400"
                  >
                    <FaLinkedin />
                  </a>
                </li>
                <li>
                  <a
                    href="https://twitter.com/johndoe"
                    target="_blank"
                    className="text-gray-400"
                  >
                    <FaTwitter />
                  </a>
                </li>
                <li>
                  <a
                    href="https://github.com/johndoe"
                    target="_blank"
                    className="text-gray-400"
                  >
                    <FaGithub />
                  </a>
                </li>
                <li>
                  <a
                    href="https://www.instagram.com/johndoe/"
                    target="_blank"
                    className="text-gray-400"
                  >
                    <FaInstagram />
                  </a>
                </li>
              </ul>
            </div>
          </aside>
        </section>
        <div className="copyright mt-8 text-center">
          <div className="copyright border-t border-gray-600 pt-4 mx-auto max-w-2xl">
            &copy; 2023 Code-Nomad. All Rights Reserved.
            <a href="/privacy-policy" className="text-gray-400">
              Privacy Policy
            </a>
          </div>
        </div>
      </div>
    </footer>
  );
}
