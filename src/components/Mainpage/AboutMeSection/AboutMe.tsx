import React from "react";
import Link from "next/link";

export default function AboutMe() {
  return (
    <section className="about bg-gray-100 text-gray-800">
      <div className="container mx-auto py-16">
        <h2 className="about-title text-3xl md:text-4xl lg:text-5xl font-bold mb-8 text-center text-blue-700">
          About Me
        </h2>
        <p className="about-description text-lg leading-relaxed mb-8 text-center">
          Welcome to my digital universe! I'm Code Nomad, a passionate front-end
          developer and tech leader, weaving intricate web experiences and
          embracing the latest technologies. Join me on this tech adventure! I
          find joy in transforming ideas into reality, and my journey in the
          ever-evolving world of technology is fueled by a curiosity to explore
          new possibilities and create meaningful solutions.
        </p>
        <div className="mb-8 text-center">
          <h3 className="text-xl font-semibold mb-2 text-blue-700">
            Coding Philosophy
          </h3>
          <p className="text-gray-700">
            I believe in crafting elegant solutions and writing code that not
            only works but is also maintainable and scalable. My mission is to
            contribute to innovative projects that make a positive impact on
            users and the tech community.
          </p>
        </div>

        <div className="flex justify-center">
          <Link
            href="/about"
            className="btn bg-blue-700 text-white py-2 px-4 rounded hover:bg-blue-800"
          >
            Discover More
          </Link>
        </div>
      </div>
    </section>
  );
}
