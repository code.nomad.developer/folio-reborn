import React from "react";

export default function HeroSection() {
  return (
    <section className="hero bg-blue-700 text-white relative">
      <div className="absolute inset-0 bg-cover bg-center z-[-1]"></div>

      <div className="container mx-auto py-12 relative text-left">
        <h1 className="text-4xl md:text-5xl lg:text-6xl font-bold leading-tight text-gray-100 mb-4">
          Explore the <span className="text-blue-400">Coding Odyssey</span> with
          Code Nomad, the Digital Explorer
        </h1>
        <h2 className="text-lg md:text-xl lg:text-2xl mb-6 text-gray-300">
          Crafting Interactive Experiences and Leading Technology Innovation for{" "}
          <span className="text-blue-500">Digital Nomads</span>
        </h2>
        <p className="text-gray-300 mb-10 text-lg text-justify">
          Embark on a thrilling journey where code meets creativity, and
          innovation takes center stage. Join me, Code Nomad, as we navigate the
          ever-changing landscape of technology, crafting interactive
          experiences that transcend boundaries. Let's explore the digital realm
          together and redefine what's possible. The Coding Odyssey awaits, and
          you, fellow Digital Nomad, are invited to be part of this
          extraordinary adventure.
        </p>

        <div className="flex items-center justify-center">
          <div className="mt-8 animate-bounce">
            <span className="text-gray-300 ">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
                className="h-8 w-8"
                alt="Arrow Down Icon"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                  d="M19 14l-7 7m0 0l-7-7m7 7V3"
                />
              </svg>
            </span>
          </div>
        </div>
      </div>
    </section>
  );
}
