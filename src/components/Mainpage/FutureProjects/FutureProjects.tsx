import Image from "next/image";
import Link from "next/link";

export default function FutureProjects() {
  const projects = [
    {
      id: 1,
      title: "Project 1",
      description: "A brief description of Project 1 goes here.",
      technologies: ["React", "Node.js"],
      category: "Web Development",
      duration: "Jan 2022 - Mar 2022",
      testimonial:
        "Working with Code Nomad was a fantastic experience. Their expertise and dedication brought our project to new heights.",
      githubRepo: "https://github.com/yourusername/project1-repo",
    },
    {
      id: 2,
      title: "Project 2",
      description: "A brief description of Project 2 goes here.",
      technologies: ["React", "Express", "MongoDB"],
      category: "Full Stack Development",
      duration: "Apr 2022 - Jun 2022",
      testimonial:
        "Code Nomad delivered outstanding results. Their innovative solutions exceeded our expectations.",
      githubRepo: "https://github.com/yourusername/project2-repo",
    },
    {
      id: 3,
      title: "Project 3",
      description: "A brief description of Project 3 goes here.",
      technologies: ["Vue.js", "Firebase"],
      category: "Front-end Development",
      duration: "Jul 2022 - Sep 2022",
      testimonial:
        "Exceptional work! Code Nomad's attention to detail and creative approach made a significant impact on our project.",
      githubRepo: "https://github.com/yourusername/project3-repo",
    },
  ];

  return (
    <section className="portfolio bg-gray-300 text-gray-800">
      <div className="container mx-auto py-16">
        <h2 className="portfolio-title text-3xl md:text-4xl lg:text-5xl font-bold mb-8 text-center text-blue-700">
          Featured Projects
        </h2>
        <p className="text-center text-gray-600 mb-8">
          Explore a journey into the future of innovation, where each project is
          a symphony of creativity and technology, pushing boundaries and
          redefining possibilities. Join me on this odyssey of imagination,
          where the future unfolds in pixels and code. Dive into the captivating
          world of tech craftsmanship, where ideas transform into seamless
          digital experiences. Witness the convergence of art and technology in
          every project, and embark on a voyage through the limitless
          possibilities that code and creativity can unfold. Your adventure into
          the world of cutting-edge solutions starts here!
        </p>

        <p className="text-center text-gray-600 mb-8">
          At the core of our future projects lies a philosophy deeply rooted in
          innovation and purpose. We believe in pushing boundaries, challenging
          the status quo, and embracing emerging technologies to shape a digital
          landscape that goes beyond conventional norms. Our approach blends a
          passion for creativity with a commitment to delivering meaningful and
          impactful solutions. As we look ahead, we envision projects that not
          only meet the needs of today but also pave the way for the
          technological landscape of tomorrow. Join us on this journey as we
          redefine the future through code and ingenuity.
        </p>

        <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-8">
          {projects.slice(0, 3).map((project) => (
            <div
              key={project.id}
              className="project-card border border-gray-400 rounded-md overflow-hidden transition-transform hover:scale-105"
            >
              <Image
                src={`https://via.placeholder.com/400x200?text=${project.title}`}
                alt={project.title}
                width="400"
                height="200"
                className="w-full h-40 object-cover"
              />
              <div className="p-6">
                <h3 className="text-xl font-bold mb-2">{project.title}</h3>
                <p className="text-gray-600">{project.description}</p>
                <div className="mt-4 flex flex-wrap">
                  {project.technologies.map((tech) => (
                    <span
                      key={tech}
                      className="bg-gray-300 text-gray-700 px-2 py-1 rounded mr-2 mb-2"
                    >
                      {tech}
                    </span>
                  ))}
                </div>
                <p className="text-gray-600 mt-2">
                  Category: {project.category}
                </p>
                <p className="text-gray-600 mt-2">
                  Duration: {project.duration}
                </p>
                <div className="mt-4">
                  <h4 className="text-lg font-semibold mb-2">
                    Client Testimonial
                  </h4>
                  <p className="text-gray-600">{project.testimonial}</p>
                </div>
                <Link
                  href={`/projects/${project.id}`}
                  className="mt-4 block text-blue-500 hover:underline transition duration-300 ease-in-out"
                >
                  Learn More
                </Link>
                <a
                  href={project.githubRepo}
                  className="mt-2 block text-blue-500 hover:underline transition duration-300 ease-in-out"
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  View on GitHub
                </a>
              </div>
            </div>
          ))}
        </div>

        {/* Button to the Projects Page */}
        <div className="flex justify-center mt-8">
          <Link
            href="/projects"
            className="btn bg-blue-700 text-white py-2 px-4 rounded hover:bg-blue-800"
          >
            View All Projects
          </Link>
        </div>
      </div>
    </section>
  );
}
