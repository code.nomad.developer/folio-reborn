import React from "react";
import { FaCode, FaCompass } from "react-icons/fa";

export default function HeroLogo() {
  return (
    <div className="flex items-center">
      <FaCode className="text-xl mr-1" />
      <div className="text-lg">Code Nomad</div>
      <FaCompass className="text-xl ml-1" />
    </div>
  );
}
