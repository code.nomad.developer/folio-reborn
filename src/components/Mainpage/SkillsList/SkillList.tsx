"use client";
import React, { useState, useEffect } from "react";
import { useSpring, animated } from "react-spring";
import { useInView } from "react-intersection-observer";

interface SkillProps {
  title: string;
  progress: number;
  description: string;
}

const Skill: React.FC<SkillProps> = ({ title, progress, description }) => {
  const [ref, inView] = useInView({
    triggerOnce: true,
  });

  const [isVisible, setIsVisible] = useState(false);

  useEffect(() => {
    if (inView) {
      setIsVisible(true);
    }
  }, [inView]);

  const props = useSpring({
    width: isVisible ? progress : 0,
  });

  return (
    <li className="mb-4" ref={ref}>
      <div className="flex items-center mb-2">
        <div className="w-6 h-6 mr-2">
          {/* You can add an icon here if desired */}
          {/* Example: <FaCode /> */}
        </div>
        <p className="font-bold">{title}</p>
      </div>
      <div className="bg-gray-300 h-4 rounded-lg">
        <animated.div
          className="h-full bg-green-500 rounded-lg relative"
          style={{ width: props.width.interpolate((width) => `${width}%`) }}
        >
          {isVisible && (
            <div className="absolute inset-0 flex items-center justify-center text-white font-bold">
              {progress}%
            </div>
          )}
        </animated.div>
      </div>
      <p className="text-gray-600 mt-2">{description}</p>
    </li>
  );
};

const SkillList: React.FC = () => {
  const skills: SkillProps[] = [
    {
      title: "Front-end Development",
      progress: 90,
      description: "Crafting user interfaces that captivate and delight users.",
    },
    {
      title: "React.js and Vue.js",
      progress: 80,
      description:
        "Building dynamic and responsive web applications with powerful JavaScript libraries.",
    },
    {
      title: "Responsive Web Design",
      progress: 85,
      description:
        "Ensuring seamless experiences across a variety of devices and screen sizes.",
    },
    {
      title: "UI/UX Design",
      progress: 75,
      description:
        "Designing intuitive and visually appealing user interfaces for optimal user experience.",
    },
    {
      title: "JavaScript (ES6+)",
      progress: 88,
      description:
        "Harnessing the power of modern JavaScript for efficient and scalable code.",
    },
    {
      title: "HTML5 and CSS3",
      progress: 92,
      description:
        "Crafting the structure and style of web pages with the latest HTML and CSS standards.",
    },
  ];

  const taglineStyle = {
    fontSize: "1.25rem", // Larger font size
    fontWeight: "bold", // Bold text
    border: "2px solid #4a5568", // Thicker border
  };

  const tagline = (
    <>
      Bringing creativity and innovation to the digital realm. Crafting seamless
      user experiences with a blend of front-end expertise, design finesse, and
      cutting-edge technologies. Let's turn ideas into engaging and interactive
      digital solutions that leave a lasting impression.
    </>
  );
  return (
    <section className="skills bg-blue-100" id="skills">
      <div className="container mx-auto py-16 flex flex-col lg:flex-row items-center">
        <div className="lg:w-1/2 pr-8">
          <h2 className="skills-title text-3xl md:text-4xl lg:text-5xl font-bold mb-8">
            My Skills
          </h2>
          <p className="text-gray-600 mb-8">
            I take pride in my diverse skill set that allows me to bring ideas
            to life with creativity and innovation. Here are some of my key
            skills:
          </p>
          <ul className="skills-list">
            {skills.map((skill, index) => (
              <Skill
                key={index}
                title={skill.title}
                description={skill.description}
                progress={skill.progress}
              />
            ))}
          </ul>
        </div>
        <div className="lg:w-1/2 mt-8 lg:mt-0">
          <div
            className="p-4 border rounded-lg text-center"
            style={taglineStyle}
          >
            <p className="text-gray-700">{tagline}</p>
          </div>
        </div>
      </div>
    </section>
  );
};

export default SkillList;
