import React from "react";
import Link from "next/link";
import Logo from "./Logo/Logo";

export default function Header() {
  return (
    <header className={`sticky top-0 z-50 bg-gray-800 text-white`}>
      <div className="container mx-auto px-2 py-2">
        <div className="flex flex-wrap justify-between items-center">
          <nav className="container mx-auto flex flex-col lg:flex-row items-center justify-between p-2">
            <Link className="text-lg font-bold mb-4 lg:mb-0 lg:mr-4" href="/">
              <Logo />
            </Link>

            <ul className="flex flex-row">
              <li className="mb-2 mr-3 lg:mb-0 lg:mr-4 uppercase transition-transform transform hover:scale-105 hover:shadow-md">
                <Link href="pages/about">About</Link>
              </li>
              <li className="mb-2 mr-3 lg:mb-0 lg:mr-4 uppercase transition-transform transform hover:scale-105 hover:shadow-md">
                <Link href="/portfolio">Portfolio</Link>
              </li>
              <li className="mb-2 mr-3 lg:mb-0 lg:mr-4 uppercase transition-transform transform hover:scale-105 hover:shadow-md">
                <Link href="/blog">Blog</Link>
              </li>
              <li className="mb-2 mr-3 lg:mb-0 lg:mr-4 uppercase transition-transform transform hover:scale-105 hover:shadow-md">
                <Link href="/contact">Contact</Link>
              </li>
              <li className="mb-2 mr-3 lg:mb-0 lg:mr-4 uppercase transition-transform transform hover:scale-105 hover:shadow-md">
                <Link
                  className="uppercase transition-transform transform hover:scale-105"
                  href="/resume"
                >
                  Resume
                </Link>
              </li>
            </ul>
          </nav>

          <div className="lg:hidden">
            <button className={` text-white`}>
              <span></span>
              <span></span>
              <span></span>
            </button>
          </div>
        </div>
      </div>
    </header>
  );
}
