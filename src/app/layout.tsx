import React from "react";
import type { Metadata } from "next";
import "./globals.css";
import Header from "@/components/Header/Header";
import Footer from "@/components/Footer/Footer";

export const metadata: Metadata = {
  title: "Code-nomad's Coding Corner | Personal Portfolio",
  description:
    "Explore the coding journey of Code-nomad, a passionate developer sharing insights, projects, and experiences.",
  keywords: [
    "coding",
    "programming",
    "web development",
    "portfolio",
    "projects",
  ],
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      <body>
        <Header />
        {children}
        <Footer />
      </body>
    </html>
  );
}
