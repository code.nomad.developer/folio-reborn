import Link from "next/link";
import {
  FaCode,
  FaPaintBrush,
  FaMobileAlt,
  FaGitAlt,
  FaServer,
} from "react-icons/fa";
import { DiReact } from "react-icons/di";
import { AiOutlineHtml5, AiOutlineAntDesign } from "react-icons/ai";
import { BsBootstrapFill } from "react-icons/bs";
import { IoLogoWordpress } from "react-icons/io5";
import { FiDatabase, FiCheckSquare, FiTarget } from "react-icons/fi";

export default function AboutPage() {
  const skillsAndExpertise = [
    { icon: <FaCode />, name: "Web Development" },
    { icon: <FaPaintBrush />, name: "UI/UX Design" },
    { icon: <FiTarget />, name: "Responsive Design" },
    { icon: <DiReact />, name: "JavaScript (React, Vue)" },
    { icon: <AiOutlineHtml5 />, name: "HTML5 & CSS3" },
    {
      icon: <BsBootstrapFill />,
      name: "Front-end Frameworks (Bootstrap, Tailwind CSS)",
    },
    { icon: <FaGitAlt />, name: "Version Control (Git)" },
    { icon: <FiDatabase />, name: "API Integration" },
    { icon: <FaMobileAlt />, name: "Mobile App Development (React Native)" },
    { icon: <FiCheckSquare />, name: "Web Performance Optimization" },
    { icon: <FaServer />, name: "Cross-Browser Compatibility" },
    { icon: <AiOutlineAntDesign />, name: "User Testing and Feedback" },
    { icon: <FiTarget />, name: "Agile Methodology" },
    {
      icon: <IoLogoWordpress />,
      name: "Graphic Design (Adobe Creative Suite)",
    },
    {
      icon: <IoLogoWordpress />,
      name: "Content Management Systems (WordPress, Contentful)",
    },
  ];
  const renderedSkills = skillsAndExpertise.map((skill, index) => (
    <div key={index} className="flex items-center mb-2">
      {skill.icon} <span className="ml-2">{skill.name}</span>
    </div>
  ));
  return (
    <div>
      <section className="about bg-gray-100 text-gray-800">
        <div className="container mx-auto py-16">
          <h2 className="about-title text-3xl md:text-4xl lg:text-5xl font-bold mb-8">
            About Me
          </h2>
          <p className="about-description text-lg leading-relaxed mb-8">
            Greetings in the realm of code! I'm Code Nomad, a dedicated
            front-end developer and tech leader. Drawing from a background in
            young eyars, I seamlessly blend creativity with technical expertise
            in every endeavor. My tech journey commenced a decade ago, unfolding
            into an exhilarating odyssey marked by challenges, continual growth,
            and the sheer delight of fashioning groundbreaking solutions. Embark
            on this tech adventure with Code Nomad, and together, let's traverse
            the boundless possibilities of the digital realm!
          </p>
        </div>
      </section>

      <section className="about bg-gray-100 text-gray-800">
        <div className="container mx-auto py-2">
          <div className="mb-8">
            <h3 className="text-xl font-semibold mb-2">My Journey</h3>
            <p>
              Over a decade ago, I set forth on a transformative journey into
              the world of programming, fueled by a profound passion for
              crafting innovative solutions. With a rich tapestry of experiences
              spanning 10 years, my path has been marked by continuous learning,
              growth, and impactful projects. One of the pivotal moments in my
              journey was the inception of a groundbreaking gamification
              project. Steering this initiative, I not only honed my technical
              skills but also witnessed the profound impact technology can have
              on shaping user experiences. This endeavor not only defined my
              trajectory but also served as a catalyst for my commitment to
              creating digital experiences that resonate. As a team leader, I
              had the privilege of guiding a dynamic team of 30 individuals.
              Navigating the challenges of collaboration and leadership, I
              cultivated an environment that fostered creativity and excellence.
              Today, as a tech lead, I bring this wealth of experience to the
              forefront, driving not only projects but also the collective
              vision of a team poised for success. Reflecting on the challenges
              that have molded me, I am grateful for the diversity of projects
              that have contributed to my skill set. Looking forward, I am not
              just excited about the projects on the horizon but passionate
              about the untapped potential waiting to be explored in the
              ever-evolving landscape of technology.
            </p>
          </div>

          <section className="skills bg-gray-200 text-gray-800">
            <div className="container mx-auto py-8">
              <h2 className="text-3xl md:text-4xl lg:text-5xl font-bold mb-4">
                Skills & Expertise
              </h2>
              <div className="grid grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-4">
                {renderedSkills}
              </div>
            </div>
          </section>

          <div className="mb-8">
            <h3 className="text-xl font-semibold mb-2">Call-to-Action</h3>
            <p>
              Ready to embark on a digital journey together? Let's collaborate
              and bring your ideas to life! Whether you have a project in mind
              or just want to connect, I'm always open to new opportunities and
              conversations. Get in touch, and let's create something amazing!
            </p>
          </div>

          <div className="flex justify-center">
            <Link
              href="/contact"
              className="btn bg-blue-700 text-white py-2 px-4 rounded hover:bg-blue-800"
            >
              Get in Touch
            </Link>
          </div>
        </div>
      </section>
    </div>
  );
}
