import React from "react";
import SkillList from "@/components/Mainpage/SkillsList/SkillList";
import FutureProjects from "@/components/Mainpage/FutureProjects/FutureProjects";
import AboutMe from "@/components/Mainpage/AboutMeSection/AboutMe";
import HeroSection from "@/components/Mainpage/HeroSection/HeroSection";

export default function Home() {
  return (
    <main className="main">
      <HeroSection />
      <AboutMe />
      <FutureProjects />
      <SkillList />
    </main>
  );
}
